import 'package:flutter/material.dart';

class MyMobileBody extends StatelessWidget {
  const MyMobileBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(title: Text('Mobile version'),centerTitle: true,),
      body: Column(children: <Widget>[
       Padding(
        padding: EdgeInsets.all(8.0),
       child: AspectRatio(
        aspectRatio: 16 / 9 ,
        child: Container(color: Colors.amber),
        ) ),
        Expanded(child: ListView.builder(
          itemCount: 8,
          itemBuilder: (context, index){
          return Padding(padding: EdgeInsets.all(8.0), child: Container(height: 120, color: Colors.amber,),);
        }))
      ]),
      
      );
  }
}